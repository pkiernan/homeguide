import { takeLatest, put } from "redux-saga/effects";
import { actionLoadApp } from "./actions";
import { LOAD_APP } from "./types";
import { callAPI } from "../../utils/services";

function* getAppData() {
  try {
    const response = yield callAPI({ url: "search/api/" });
    yield put(actionLoadApp.success(response));
  } catch (e) {
    yield put(actionLoadApp.fail(e));
  }
}

export function* watchAppLoad() {
  yield takeLatest(LOAD_APP.CALL, getAppData);
}
