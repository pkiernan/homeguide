### Template Information

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and extended by [Paul Kiernan](<[https://bitbucket.org/pkiernan](https://bitbucket.org/pkiernan)>).

#### Available Scripts

In the project directory, you can run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### Technologies

- React
- Redux
- Redux-Saga
- Reselect
- Axios
