import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { selectorIsLoaded } from "./state/app/selectors";
import { actionLoadApp } from "./state/app/actions";
import { Switch, Route } from "react-router-dom";
import { DetailPage } from "./pages/Detail";
import { HomePage } from "./pages/Home";
import LoadingWheel from "./components/LoadingWheel";
import "./styles/app.scss";

class App extends Component {
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    loadApp: PropTypes.func.isRequired
  };
  componentDidMount() {
    const { loadApp } = this.props;
    loadApp();
  }
  renderHeader() {
    return (
      <header className="app-header">
        <h1 className="logo">Home Guide</h1>
      </header>
    );
  }
  renderFooterNav() {
    return (
      <nav className="app-footer__nav">
        <li>
          <a href="/">Lorem Ipsum</a>
        </li>
        <li>
          <a href="/">Dolor</a>
        </li>
        <li>
          <a href="/">Sit Amet</a>
        </li>
        <li>
          <a href="/">Consectetur</a>
        </li>
        <li>
          <a href="/">Adipiscing</a>
        </li>
      </nav>
    );
  }
  renderFooterInfo() {
    return (
      <div className="app-footer__info">
        <span>&copy; Paul Kiernan</span>
        <span>
          This site is a tech test by Paul Kiernan focusing on good design
          standards.
        </span>
        <span>
          Check out{" "}
          <a
            href="https://bitbucket.org/%7B1b41a916-bb58-4e90-9747-1891a821e6d3%7D/"
            target="_blank_"
          >
            my Bitbucket account
          </a>{" "}
          for more.
        </span>
      </div>
    );
  }
  renderFooterContact() {
    return (
      <div className="app-footer__info">
        <span>Paul Kiernan</span>
        <span>
          <a href="mailto:xpaulk@gmail.com">xpaulk@gmail.com</a>
        </span>
        <span>
          <a href="tel:+44 (0) 7540 879 646">+44 (0) 7540 879 646</a>
        </span>
      </div>
    );
  }
  renderFooter() {
    return (
      <footer className="app-footer container">
        <h1 className="logo">Home Guide</h1>
        <div className="columns">
          {this.renderFooterNav()}
          {this.renderFooterInfo()}
          {this.renderFooterContact()}
        </div>
      </footer>
    );
  }
  renderContent() {
    const { isLoaded } = this.props;
    return isLoaded ? (
      <Fragment>
        {this.renderHeader()}
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/detail" component={DetailPage} />
        </Switch>
        {this.renderFooter()}
      </Fragment>
    ) : null;
  }
  renderLoading() {
    const { isLoaded } = this.props;
    return !isLoaded ? <LoadingWheel /> : null;
  }
  render() {
    return (
      <div className="App">
        {this.renderContent()}
        {this.renderLoading()}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadApp: () => dispatch(actionLoadApp.call())
});

const mapStateToProps = state => ({
  isLoaded: selectorIsLoaded(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
