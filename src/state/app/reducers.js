import { get } from "lodash";
import { LOAD_APP } from "./types";

const initState = {
  isLoaded: false,
  currencyId: null,
  currencySymbol: null,
  currencyRate: null,
  listings: []
};

const formatSucess = (data = {}) => {
  const state = {
    currencyId: get(data, "Metrics.Currency.CurencyId", null),
    currencySymbol: get(data, "Metrics.Currency.Symbol", null),
    currencyRate: get(data, "Metrics.Currency.FxRate", 1),
    listings: get(data, "Listings", [])
  };
  return state;
};

export default (state = initState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case LOAD_APP.CALL: {
      return {
        ...state,
        isLoaded: false
      };
    }
    case LOAD_APP.SUCCESS: {
      return {
        ...state,
        isLoaded: true,
        ...formatSucess(payload.data)
      };
    }
    case LOAD_APP.FAIL: {
      return {
        ...state,
        isLoaded: true
      };
    }
    default:
      return state;
  }
};
