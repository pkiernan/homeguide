import { SEARCH_LISTINGS } from "./types";
import { createAction } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const actionSearchListings = createAction({
  id: SEARCH_LISTINGS,
  type: ACTION_API
});
