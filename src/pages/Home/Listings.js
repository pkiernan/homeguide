import React, { Component } from "react";
import PropTypes from "prop-types";
import Image from "../../components/Image";
import LoadingWheel from "../../components/LoadingWheel";

import "./styles.scss";

class Listings extends Component {
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    listings: PropTypes.array
  };
  static defaultProps = {
    isLoaded: false,
    listings: []
  };
  renderLoading() {
    return <LoadingWheel />;
  }
  renderResults() {
    const { listings } = this.props;
    const listingComponents = listings.map(
      ({
        id,
        name,
        image,
        bedroomCount,
        bathroomCount,
        guestCount,
        area,
        displayPrice
      }) => (
        <article className="listing" key={id}>
          <header>
            <Image url={image} />
            <h1>{name}</h1>
          </header>
          <div className="listing-info">
            <div className="info">
              <h2>{area}</h2>
              <dl>
                <dt>Bedrooms:</dt>
                <dd>{bedroomCount}</dd>
                <dt>Bathrooms:</dt>
                <dd>{bathroomCount}</dd>
                <dt>Guests:</dt>
                <dd>{guestCount}</dd>
              </dl>
            </div>
            <span className="price">{`${displayPrice}/night`}</span>
          </div>
        </article>
      )
    );
    return <div className="listings-list">{listingComponents}</div>;
  }
  render() {
    const { isLoaded } = this.props;
    return isLoaded ? (
      <section className="listings">
        <h1>Listings</h1>
        {this.renderResults()}
      </section>
    ) : (
      this.renderLoading()
    );
  }
}
export default Listings;
