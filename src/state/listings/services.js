import { takeLatest, put, select } from "redux-saga/effects";
import { actionSearchListings } from "./actions";
import { SEARCH_LISTINGS } from "./types";
import { callAPI } from "../../utils/services";
import { selectFilterOptions } from "./selectors";

function* getListings() {
  try {
    const params = yield select(selectFilterOptions);
    const response = yield callAPI({ url: "search/api/", params });
    yield put(actionSearchListings.success(response));
  } catch (e) {
    yield put(actionSearchListings.fail(e));
  }
}

export function* watchSearchListings() {
  yield takeLatest(SEARCH_LISTINGS.CALL, getListings);
}
