import { get } from "lodash";
import { SEARCH_LISTINGS } from "./types";

const initState = {
  isLoaded: false,
  isInitiallySearched: false,
  listings: []
};

export default (state = initState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case SEARCH_LISTINGS.CALL: {
      return {
        ...initState,
        isInitiallySearched: true
      };
    }
    case SEARCH_LISTINGS.SUCCESS: {
      return {
        ...state,
        isLoaded: true,
        listings: get(payload, "data.Listings", [])
      };
    }
    case SEARCH_LISTINGS.FAIL: {
      return {
        ...state,
        isLoaded: true
      };
    }
    default:
      return state;
  }
};
