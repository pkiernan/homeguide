import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { selectorPrimeFeatureImage } from "../../state/app/selectors";
import {
  selectorIsInitialllySearched,
  selectorListings,
  selectorIsLoaded as selectorIsListingsLoaded
} from "../../state/listings/selectors";
import Image from "../../components/Image";
import Filter from "../../components/Filter";
import Listings from "./Listings";
import "./styles.scss";

class HomePage extends Component {
  static propTypes = {
    primeImage: PropTypes.string,
    isInitiallySearched: PropTypes.bool.isRequired
  };
  renderFeatureImage() {
    const { primeImage } = this.props;
    return primeImage && primeImage.length ? (
      <div className="feature-image">
        <Image url={primeImage} />
      </div>
    ) : null;
  }
  renderFilter() {
    return <Filter />;
  }
  renderWelcome() {
    const { isInitiallySearched } = this.props;
    return !isInitiallySearched ? (
      <Fragment>
        <h1>Welcome To Home Guide</h1>
        <article className="editorial">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Pellentesque scelerisque purus cursus, consectetur quam vitae,
            imperdiet odio. Nam non accumsan orci, et rutrum sapien. Vestibulum
            ante ipsum primis in faucibus orci luctus et ultrices posuere
            cubilia Curae; Nunc massa nunc, efficitur ut consectetur a,
            elementum a nibh. Ut tincidunt eleifend accumsan.
          </p>
          <p>
            Proin ac ornare orci. Mauris augue quam, iaculis in rhoncus ut,
            vestibulum nec lorem. In facilisis auctor ullamcorper. Phasellus
            rutrum blandit justo, lacinia dapibus nisl laoreet sagittis. Quisque
            non quam blandit, fermentum quam quis, vestibulum ante. Mauris sit
            amet tristique leo.
          </p>
        </article>
      </Fragment>
    ) : null;
  }
  renderListings() {
    const { isInitiallySearched, listings, isListingsLoaded } = this.props;
    return isInitiallySearched ? (
      <Listings listings={listings} isLoaded={isListingsLoaded} />
    ) : null;
  }
  render() {
    const { isInitiallySearched } = this.props;
    const className = `page home-page ${
      isInitiallySearched ? "with-results" : null
    }`;
    return (
      <section className={className}>
        {this.renderFeatureImage()}
        <main className="container">
          {this.renderFilter()}
          {this.renderWelcome()}
          {this.renderListings()}
        </main>
      </section>
    );
  }
}

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => ({
  primeImage: selectorPrimeFeatureImage(state),
  isInitiallySearched: selectorIsInitialllySearched(state),
  isListingsLoaded: selectorIsListingsLoaded(state),
  listings: selectorListings(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
