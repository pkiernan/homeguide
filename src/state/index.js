import { combineReducers } from "redux";
import { reducer as app } from "./app";
import { reducer as listings } from "./listings";
import { reducer as form } from "redux-form";

export default combineReducers({
  app,
  listings,
  form
});
