import { createActionType } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const SEARCH_LISTINGS = createActionType({
  id: "SEARCH_LISTINGS",
  type: ACTION_API
});
