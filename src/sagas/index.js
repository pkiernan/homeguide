import { all, fork } from "redux-saga/effects";
import { watchAppLoad } from "../state/app/services";
import { watchSearchListings } from "../state/listings/services";

export default function* root() {
  yield all([fork(watchAppLoad), fork(watchSearchListings)]);
}
