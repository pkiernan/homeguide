import { createSelector } from "reselect";
import { get } from "lodash";
import { formatListing } from "../../utils/formatter";

const getState = ({ app = {} }) => app;

export const selectorIsLoaded = createSelector(
  getState,
  ({ isLoaded }) => isLoaded === true
);
export const selectorCurrencyId = createSelector(
  getState,
  ({ currencyId }) => currencyId
);
export const selectorCurrencySymbol = createSelector(
  getState,
  ({ currencySymbol }) => currencySymbol
);
export const selectorCurrencyRate = createSelector(
  getState,
  ({ currencyRate }) => currencyRate
);
export const selectorListingsRaw = createSelector(
  getState,
  ({ listings = [] }) => listings
);
export const selectorListings = createSelector(
  [selectorListingsRaw, selectorCurrencySymbol, selectorCurrencyRate],
  (listings, symbol, rate) => formatListing({ listings, symbol, rate })
);
export const selectorPrimeFeatureImage = createSelector(
  selectorListings,
  (listings = []) => get(listings, "[0].image", null)
);
