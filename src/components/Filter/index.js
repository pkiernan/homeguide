import React, { Component } from "react";
import { reduxForm } from "redux-form";
import PropTypes from "prop-types";
import { FIELD_INPUT, FIELD_SELECT } from "../../constants";
import { Field } from "../Form";
import { Submit } from "../Form";
import { connect } from "react-redux";
import { selectorCurrencySymbol } from "../../state/app/selectors";
import { actionSearchListings } from "../../state/listings/actions";
import "./styles.scss";

/*
  DEVCOMMENT
  Ideally I'd like to add some validation here, particularly around the inputted fields (making sure max > min etc.)
  However, running out of time and focusing my efforts on layout and design choices as per the brief
*/

class Filter extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    searchListings: PropTypes.func.isRequired,
    currencySymbol: PropTypes.string.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    className: PropTypes.string
  };
  static defaultProps = {
    currencySymbol: "£",
    pristine: true,
    valid: false,
    className: ""
  };
  renderCity() {
    const cities = ["London", "Paris", "New York"];
    const props = {
      name: "city",
      componentType: FIELD_SELECT,
      attributes: {
        label: "City:",
        placeholder: "Select city",
        options: cities.map((city, index) => ({
          label: city,
          value: index + 1
        }))
      }
    };
    return <Field key={"city"} {...props} />;
  }
  renderGuests() {
    const count = 11;
    const props = {
      name: "guests",
      componentType: FIELD_SELECT,
      attributes: {
        label: "Guests:",
        placeholder: "Select Guests",
        options: new Array(count).fill(null).map((val, index) => {
          const guestLabel = index === 0 ? "Guest" : "Guests";
          const numberLabel = index === count - 1 ? `${index + 1}+` : index + 1;
          return {
            label: `${numberLabel} ${guestLabel}`,
            value: index + 1
          };
        })
      }
    };
    return <Field key={"guests"} {...props} />;
  }
  renderMin() {
    const { currencySymbol } = this.props;
    const props = {
      name: "min",
      componentType: FIELD_INPUT,
      normalize: value => Math.abs(value),
      attributes: {
        type: "number",
        min: 0,
        label: `Min Price (${currencySymbol}):`,
        placeholder: `${currencySymbol}0`
      }
    };
    return <Field key="min" {...props} />;
  }
  renderMax() {
    const { currencySymbol } = this.props;
    const props = {
      name: "max",
      componentType: FIELD_INPUT,
      normalize: value => Math.abs(value),
      attributes: {
        type: "number",
        min: 0,
        label: `Max Price (${currencySymbol}):`,
        placeholder: `${currencySymbol}0`
      }
    };
    return <Field key="max" {...props} />;
  }
  renderSubmit() {
    return <Submit label="Search" disabled={!this.isFormValid()} />;
  }
  isFormValid() {
    const { pristine, valid } = this.props;
    return !pristine && valid;
  }
  onSubmit() {
    const { searchListings } = this.props;
    if (this.isFormValid()) {
      searchListings();
    }
    return false;
  }
  render() {
    const { handleSubmit, className } = this.props;
    return (
      <aside className={`filter ${className}`}>
        <form
          className="container"
          action="/"
          onSubmit={handleSubmit(this.onSubmit.bind(this))}
        >
          {this.renderCity()}
          {this.renderGuests()}
          {this.renderMin()}
          {this.renderMax()}
          {this.renderSubmit()}
        </form>
      </aside>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  searchListings: () => dispatch(actionSearchListings.call())
});

const mapStateToProps = state => ({
  currencySymbol: selectorCurrencySymbol(state)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: "filter"
  })(Filter)
);
