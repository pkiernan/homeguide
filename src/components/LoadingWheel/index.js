import React, { Component } from "react";
import "./styles.scss";

class LoadingWheel extends Component {
  render() {
    return <span className="loader" />;
  }
}
export default LoadingWheel;
