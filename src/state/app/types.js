import { createActionType } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const LOAD_APP = createActionType({ id: "LOAD_APP", type: ACTION_API });
