import { createSelector } from "reselect";
import { selectorCurrencySymbol, selectorCurrencyRate } from "../app/selectors";
import { formatListing } from "../../utils/formatter";

const getState = ({ listings = {} }) => listings;
const getForm = ({ form = {} }) => form.filter;

const mapApiData = {
  city: "CityId",
  guests: "Guests",
  min: "PriceBottom",
  max: "PriceT0"
};

export const selectorIsLoaded = createSelector(
  getState,
  ({ isLoaded }) => isLoaded === true
);
export const selectorIsInitialllySearched = createSelector(
  getState,
  ({ isInitiallySearched }) => isInitiallySearched === true
);
export const selectorListingsRaw = createSelector(
  getState,
  ({ listings = [] }) => listings
);
export const selectorListings = createSelector(
  [selectorListingsRaw, selectorCurrencySymbol, selectorCurrencyRate],
  (listings, symbol, rate) => formatListing({ listings, symbol, rate })
);
export const selectFilterValues = createSelector(
  getForm,
  ({ values = {} }) => values
);
export const selectFilterOptions = createSelector(selectFilterValues, values =>
  Object.keys(values).reduce(
    (agg, key) => ({
      ...agg,
      [mapApiData[key]]: Number(values[key])
    }),
    {}
  )
);
