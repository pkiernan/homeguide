export const convertToCurrency = ({ value, symbol }) => {
  const options = {
    maximumFractionDigits: 2,
    minimumFractionDigits: value % 1 === 0 ? 0 : 2
  };
  const price = value.toLocaleString({}, options);
  return `${symbol}${price}`;
};
