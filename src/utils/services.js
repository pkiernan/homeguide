import axios from "axios";

const baseUrl = "https://plumguide-staging.global.ssl.fastly.net";

export function* callAPI({ url = "", method = "get", data = {}, params = {} }) {
  const config = {
    method,
    data,
    params,
    url: `${baseUrl}/${url}`,
    headers: {
      "content-type": "application/json"
    }
  };
  return yield axios(config);
}
