import React, { Component } from "react";
import { Field } from "redux-form";
import PropTypes from "prop-types";
import { FIELD_INPUT, FIELD_SELECT } from "../../constants";
import Input from "./Input";
import Select from "./Select";
import "./styles.scss";

class SelectComponent extends Component {
  render() {
    const { input, ...attributes } = this.props;
    const { name, onChange } = input;
    const props = {
      name,
      onChange,
      ...attributes
    };
    return <Select {...props} />;
  }
}
class TextComponent extends Component {
  render() {
    const { input, ...attributes } = this.props;
    const { name, onChange } = input;
    const props = {
      name,
      onChange,
      ...attributes
    };
    return <Input {...props} />;
  }
}

class FieldComponent extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    attributes: PropTypes.object,
    componentType: PropTypes.string.isRequired,
    normalize: PropTypes.func
  };
  static defaultProps = {
    name: "",
    attributes: {},
    componentType: FIELD_INPUT,
    normalize: value => value
  };
  render() {
    const { componentType, attributes, name, normalize } = this.props;
    let component;
    switch (componentType) {
      case FIELD_SELECT:
        component = SelectComponent;
        break;
      case FIELD_INPUT:
      default:
        component = TextComponent;
    }
    return (
      <Field
        name={name}
        component={component}
        normalize={normalize}
        props={attributes}
      />
    );
  }
}

export default FieldComponent;
