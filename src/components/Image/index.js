import React, { Component } from "react";
import PropTypes from "prop-types";
import "./styles.scss";

class Image extends Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    className: PropTypes.string
  };
  static defaultProps = {
    className: ""
  };
  render() {
    const { className, url } = this.props;
    const style = {
      backgroundImage: `url(${url})`
    };
    return <div className={`image ${className}`} style={style} />;
  }
}
export default Image;
