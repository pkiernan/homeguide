import React, { Component } from "react";
import PropTypes from "prop-types";
import "./styles.scss";

class Submit extends Component {
  static propTypes = {
    label: PropTypes.string,
    disabled: PropTypes.bool
  };
  static defaultProps = {
    label: "Submit",
    disabled: false
  };
  render() {
    const { label, disabled } = this.props;
    return (
      <span className="form_element form_submit">
        <button disabled={disabled} type="submit">
          {label}
        </button>
      </span>
    );
  }
}

export default Submit;
