import React, { Component } from "react";
import PropTypes from "prop-types";
import "./styles.scss";

class Input extends Component {
  static propTypes = {
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    onChange: PropTypes.func
  };
  static defaultProps = {
    label: "",
    placeholder: null,
    type: "text"
  };
  render() {
    const { label, ...attribs } = this.props;
    return (
      <span className="form_element form_input">
        <label>
          {label}
          <input {...attribs} />
        </label>
      </span>
    );
  }
}

export default Input;
