import React, { Component } from "react";
import PropTypes from "prop-types";
import "./styles.scss";

class Select extends Component {
  static propTypes = {
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
          .isRequired
      })
    ),
    onChange: PropTypes.func.isRequired
  };
  static defaultProps = {
    label: "",
    placeholder: null,
    options: []
  };
  renderOptions() {
    const { placeholder, options } = this.props;
    return [
      placeholder && placeholder.length ? (
        <option key="placeholder" value="">
          {placeholder}
        </option>
      ) : null,
      ...options.map(({ label, value }) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))
    ];
  }
  render() {
    const { label, name, onChange } = this.props;
    return (
      <span className="form_element form_select">
        <label>
          {label}
          <select onChange={onChange} name={name}>
            {this.renderOptions()}
          </select>
        </label>
      </span>
    );
  }
}

export default Select;
