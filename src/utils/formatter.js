import { get } from "lodash";
import { convertToCurrency } from "./calculation";

export const formatListing = ({ listings, symbol, rate }) =>
  listings.map(listing => {
    const priceIncFees = get(listing, "DisplayPriceWithFees");
    const convertedPrice = priceIncFees * rate;
    return {
      id: get(listing, "ListingId"),
      name: get(listing, "Name"),
      price: get(listing, "TotalPrice"),
      priceIncFees,
      displayPrice: convertToCurrency({ value: convertedPrice, symbol }),
      bedroomCount: get(listing, "BedroomCount"),
      bathroomCount: get(listing, "BathroomCount"),
      guestCount: get(listing, "Guests"),
      cityName: get(listing, "CityName"),
      image: get(listing, "FeaturedImage"),
      area: `${get(listing, "NeighbourhoodName")}, ${get(listing, "CityName")}`
    };
  });
